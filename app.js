var express = require('express')
var bodyParser = require('body-parser')
var path = require('path');
var app = express()
var port = 3000;
var fs = require('fs');
var flights = [
    {
        "price" : 2055,
        "airline" : "El Al Israel Air",
        "takeoff" : "BKK 15:35",
        "landing" : "CDG 05:30",
        "stop" : "1(19h 55m)"
    },
    {
        "price" : 3519,
        "airline" : "Singapore Air",
        "takeoff" : "BKK 19:05",
        "landing" : "CDG 06:50",
        "stop" : "1(17h 45m)"
    },
    {
        "price" : 3519,
        "airline" : "Singapore Air",
        "takeoff" : "BKK 19:05",
        "landing" : "CDG 06:50",
        "stop" : "1(17h 45m)"
    },
    {
        "price" : 5,
        "airline" : "Thailand Airline",
        "takeoff" : "1 โมง",
        "landing" : "2 โมง",
        "stop" : "กี่โมงก็ได้"
    },
    {
      "price" : 5,
      "airline" : "Laos Airline",
      "takeoff" : "3 โมง",
      "landing" : "4 โมง",
      "stop" : "กี่โมงก็ได้"

    },
    {
      "price" : 5,
      "airline" : "Indonesia Airline",
      "takeoff" : "5 โมง",
      "landing" : "6 โมง",
      "stop" : "กี่โมงก็ได้"

    },
    {
        "price" : 5,
        "airline" : "Indonesia Airline",
        "takeoff" : "5 โมง",
        "landing" : "6 โมง",
        "stop" : "กี่โมงก็ได้"
  
      },
      {
        "price" : 5,
        "airline" : "Indonesia Airline",
        "takeoff" : "5 โมง",
        "landing" : "6 โมง",
        "stop" : "กี่โมงก็ได้"
  
      },
      {
        "price" : 5,
        "airline" : "Indonesia Airline",
        "takeoff" : "5 โมง",
        "landing" : "6 โมง",
        "stop" : "กี่โมงก็ได้"
  
      },
      {
        "price" : 1000,
        "airline" : "Korean Airline",
        "takeoff" : "2 โมง",
        "landing" : "6 โมง",
        "stop" : "กี่โมงก็ได้ โตแล้ว"
  
      },
  ];

app.use('/static',express.static(path.join(__dirname + "/public")));

app.get('/',function(req,res){
    res.sendFile(path.join(__dirname + "/searchFlights.html"));
});
app.get('/searchFlights',function(req,res){
    fs.readFile('displayFlight.html',function(err,text){
        res.end(text);
    });
});
app.get('/loadData/:id',function(req,res){
    //console.log('enter Data');
    var arr = [];
    const numberPerpage = 3;
    var requestPage = req.params.id-1;
    console.log('request : ' + requestPage);
    var counter = 0;
    
        for(let i = requestPage*numberPerpage ; i < requestPage*numberPerpage+3 ; i++){
            if(flights[i] != undefined)
                arr.push(flights[i]);
        }
    console.log(arr);
    res.send(arr);
    
});
app.get('/addPage',function(req,res){
    res.send(flights.length+'');
});

app.listen(port,function(){
    console.log("connected at port : " + port);
})
